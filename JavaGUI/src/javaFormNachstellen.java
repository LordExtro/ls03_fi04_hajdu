import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;


public class javaFormNachstellen extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtEingeben;
	private JLabel lblDieserTextSoll;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					javaFormNachstellen frame = new javaFormNachstellen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the frame.
	 */
	public javaFormNachstellen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 390, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 489, 359, 62);
		contentPane.add(btnExit);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(10, 424, 106, 23);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(134, 424, 106, 23);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(264, 424, 100, 23);
		contentPane.add(btnRechtsbndig);
		
		JButton buttonGroesser = new JButton("+");
		buttonGroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font f = lblDieserTextSoll.getFont();
				lblDieserTextSoll.setFont(f.deriveFont(f.getStyle(), f.getSize() + 1));
			}
		});
		buttonGroesser.setBounds(10, 359, 181, 23);
		contentPane.add(buttonGroesser);
		
		JButton btnSchriftRot = new JButton("Rot");
		btnSchriftRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnSchriftRot.setBounds(10, 294, 106, 23);
		contentPane.add(btnSchriftRot);
		
		JButton btnSchriftBlau = new JButton("Blau");
		btnSchriftBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnSchriftBlau.setBounds(134, 294, 106, 23);
		contentPane.add(btnSchriftBlau);
		
		JButton btnSchriftSchwarz = new JButton("Schwarz");
		btnSchriftSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchriftSchwarz.setBounds(264, 294, 100, 23);
		contentPane.add(btnSchriftSchwarz);
		
		txtEingeben = new JTextField();
		txtEingeben.setText("Hier bitte Text eingeben");
		txtEingeben.setBounds(10, 226, 354, 20);
		contentPane.add(txtEingeben);
		txtEingeben.setColumns(10);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Arial", Font.PLAIN,lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnArial.setBounds(10, 198, 106, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Comic Sans MS", Font.PLAIN, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnComicSansMs.setBounds(134, 198, 106, 23);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Courier New", Font.PLAIN,lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(264, 198, 100, 23);
		contentPane.add(btnCourierNew);
		
		JButton btnHinterGelb = new JButton("Gelb");
		btnHinterGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnHinterGelb.setBounds(10, 133, 106, 23);
		contentPane.add(btnHinterGelb);
		
		JButton btnHinterStandardfarbe = new JButton("Standardfarbe");
		btnHinterStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color(0xEEEEEE));
			}
		});
		btnHinterStandardfarbe.setBounds(134, 133, 106, 23);
		contentPane.add(btnHinterStandardfarbe);
		
		JButton btnHinterFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnHinterFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255)));
			}
		});
		btnHinterFarbeWhlen.setBounds(264, 133, 100, 23);
		contentPane.add(btnHinterFarbeWhlen);
		
		JButton btnHinterRot = new JButton("Rot");
		btnHinterRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnHinterRot.setBounds(10, 99, 106, 23);
		contentPane.add(btnHinterRot);
		
		JButton btnHinterGrn = new JButton("Gr\u00FCn");
		btnHinterGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnHinterGrn.setBounds(134, 99, 106, 23);
		contentPane.add(btnHinterGrn);
		
		JButton btnHinterBlau = new JButton("Blau");
		btnHinterBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnHinterBlau.setBounds(264, 99, 100, 23);
		contentPane.add(btnHinterBlau);
		
		JLabel lblProgrammBeenden = new JLabel("Aufgabe 6: Programm beenden");
		lblProgrammBeenden.setBounds(13, 464, 178, 20);
		contentPane.add(lblProgrammBeenden);
		
		JLabel lblTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblTextausrichtung.setBounds(12, 400, 162, 21);
		contentPane.add(lblTextausrichtung);
		
		JLabel lblSchriftgroesse = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblSchriftgroesse.setBounds(11, 339, 194, 14);
		contentPane.add(lblSchriftgroesse);
		
		JLabel lblSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblSchriftfarbe.setBounds(10, 272, 163, 20);
		contentPane.add(lblSchriftfarbe);
		
		JLabel lblText = new JLabel("Aufgabe 2: Text formatieren");
		lblText.setBounds(10, 173, 164, 20);
		contentPane.add(lblText);
		
		JLabel lblHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblHintergrundfarbe.setBounds(10, 79, 195, 15);
		contentPane.add(lblHintergrundfarbe);
		
		lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblDieserTextSoll.setForeground(Color.BLACK);
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		lblDieserTextSoll.setBounds(10, 11, 354, 62);
		contentPane.add(lblDieserTextSoll);
		
		JButton buttonKleiner = new JButton("-");
		buttonKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//int groesse = lblDieserTextSoll.getFont().getSize();
				Font f = lblDieserTextSoll.getFont();
				lblDieserTextSoll.setFont(f.deriveFont(f.getStyle(), f.getSize() - 1));
			}
		});
		buttonKleiner.setBounds(201, 359, 168, 23);
		contentPane.add(buttonKleiner);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label Schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText(txtEingeben.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 249, 181, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnTextImLabel.setBounds(193, 249, 171, 23);
		contentPane.add(btnTextImLabel);
	}
}
