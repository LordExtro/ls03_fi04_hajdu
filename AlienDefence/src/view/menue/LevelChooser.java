package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import model.persistance.IPersistance;
import model.persistanceDummy.PersistanceDummy;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;

	
	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, String quelle) {
		setBackground(Color.BLACK);
		setForeground(Color.GREEN);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		this.alienDefenceController = alienDefenceController;
		BorderLayout borderLayout = new BorderLayout();
		setLayout(borderLayout);

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setForeground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked();
			}
		});
		pnlButtons.add(btnSpielen);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Level> arrLevel = lvlControl.readAllLevels();
				
				int selectedRow;
				selectedRow = tblLevels.getSelectedRow();
				
				new Highscore(alienDefenceController.getAttemptController(), arrLevel.get(selectedRow).getLevel_id());
			}
		});
		pnlButtons.add(btnHighscore);
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		if(quelle.equals("Spielen")) {
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnNewLevel.setVisible(false);
		}
		else if(quelle.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
			btnHighscore.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}


	public void btnSpielen_Clicked() {
		
		Thread t = new Thread("GameThread") {
			@Override
			public void run() {
				User user = new User(1, "", "");
				
				List<Level> arrLevel = lvlControl.readAllLevels();
				
				int selectedRow;
				selectedRow = tblLevels.getSelectedRow();
				GameController gameController = alienDefenceController.startGame(arrLevel.get(selectedRow), user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
		
	}
}

